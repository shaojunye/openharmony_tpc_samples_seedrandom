## path-to-regexp单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/tdegrunt/jsonschema/tree/master/test) 进行单元测试

### 单元测试用例覆盖情况

|类名 |接口名 | 是否通过 |备注|
|---|---|---|---|
| |validate | pass ||
|RewriteFunction |rewrite | pass ||
|PreValidatePropertyFunction |preValidateProperty | pass ||
|CustomFormat |customFormats | pass ||
|CustomProperty |attributes | pass ||
|Validator|addSchema|pass||
||validate|pass||
|ValidatorResult|addError|pass||
||toString|pass||
|ValidationError|toString|pass||