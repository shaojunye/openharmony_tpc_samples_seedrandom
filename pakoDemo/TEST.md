## pako单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/nodeca/pako/tree/master/test) 进行单元测试

### 单元测试用例覆盖情况

| 接口名                | 是否通过 |备注|
|--------------------|---|---|
| Deflate()     |pass|
| push()             |pass|
| err                |pass|
| deflate()          |pass|
| ZStream()          |pass|
| deflateInit()      |pass|
| deflateSetHeader() |pass|
| deflateEnd()       |pass|
| gzip()        |pass|
| deflateRaw()  |pass|
| Inflate()     |pass|
| inflate()     |pass|
| result    |pass|
| ungzip()      |pass|
| inflate_table()    |pass|