## v6.2.1

1.本库是基于path-to-regexp源库v6.2.1版本代码进行OpenHarmony环境接口验证的三方库。
2.path-to-regexp支持将路径字符串(如/user/:name)转换为正则表达式，解析路径中的参数或者拼接路径。
3.包管理工具由npm切换为ohpm
适配DevEco Studio:  3.1 Beta2(3.1.0.400)
适配SDK: API9 Release(3.2.11.9)

## 1.0.0

1. 发布path-to-regexp的适配鸿蒙系统的sample demo.




