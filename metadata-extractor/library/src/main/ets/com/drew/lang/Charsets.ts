/*
Copyright (c) 2022 Huawei Device Co., Ltd.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

class Charsets {
  public static UTF_8 = "UTF-8".toString();
  public static UTF_16 = "UTF-16".toString();
  public static ISO_8859_1 = "ISO-8859-1".toString();
  public static ASCII = "US-ASCII".toString();
  public static UTF_16BE = "UTF-16BE".toString();
  public static UTF_16LE = "UTF-16LE".toString();
  public static WINDOWS_1252 = "Cp1252".toString();
}

export default Charsets