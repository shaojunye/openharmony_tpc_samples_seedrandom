# tinyxml2Demo

## 介绍
tinyxml2Demo为三方库[tinyxml2](https://github.com/leethomason/tinyxml2)(simple、small、efficient 的开源 C++ XML 文件解析库，可以很方便地应用到现有的项目中。)的应用hap示例demo。

## 下载tinyxml2

进入openharmony_tpc_samples/tinyxml2/entry/src/main/cpp/thirdparty/目录，执行命令

```shell
git clone https://github.com/leethomason/tinyxml2.git
```

## 约束与限制
在下述版本验证通过：

DevEco Studio: 4.0 Canary(4.0.0.112), SDK: API10 (4.0.7.5)

## 目录结构
````
|----ohos_mqtt  
|     |---- entry  # 示例代码文件夹
|         |---- cpp # c/c++和napi代码
|            |---- thirdparty # 三方库tinyxml代码
|            |---- CMakeLists.txt  # 构建脚本
|         |---- ets # demo代码                   
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/blob/master/tinyxml2Demo/LICENSE) ，请自由地享受和参与开源。 
    



