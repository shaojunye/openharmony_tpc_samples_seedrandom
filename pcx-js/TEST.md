## pcx-js单元测试用例

该测试用例基于OpenHarmony系统，获取pcx图像格式数据源，并且解析为像素数据的测试用例。

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|new PCX(buffer)|pass|初始化pcx对象|
|decode|pass|调用解码接口|