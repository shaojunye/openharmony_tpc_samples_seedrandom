### 2.0.0

1.适配DevEco Studio:4.1 Canary(4.1.3.317)，OpenHarmony SDK:API version 11 (4.1.0.36)

### 1.0.0

1. 样例适配的系统版本是 OpenHarmony API 9.
2. 使用三方库 node-rules 版本支持@7.7.0.
3. 轻量级的正向链接规则引擎功能样例的实现.
